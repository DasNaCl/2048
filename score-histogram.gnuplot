set terminal postscript eps color solid
set output 'plot.eps'

set boxwidth 0.8

set style fill solid

set yrange [1:*]

unset key

fit a '< sort -g data | awk NF | uniq -c' via a
plot '< sort -g data | awk NF | uniq -c' with boxes, a t "ø"
