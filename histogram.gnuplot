set terminal postscript eps color solid
set output 'plot.eps'

set style data histogram
set style histogram rowstacked

set boxwidth 0.8

set style fill solid

set yrange [1:*]

unset key

fit a '< sort -g data' via a

set label 1 gprintf("Durchschnittlich %g", a) at screen 0.15, 0.8 left

plot '< sort -g data | uniq -c' using 1:xtic(2) with histograms

