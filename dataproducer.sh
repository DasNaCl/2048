#!/bin/bash

function execute
{
    num="$1"
    method="$2"
    printer="$3"

    i=1
    while [ "$i" -le "$num" ]
    do
        ./bin/Release/a2 -dpw $printer $method

        i=$(($i + 1))
    done
}

method=$1
printer=$2
dr=$3

if [ -z "$method" ] ; then
    echo "Please provide the method."
    exit
fi

if [ -z "$printer" ] ; then
    echo "Please provide the printer."
    exit 
fi

re='^[1-9]+[0-9]*$'
if [ -z "$dr" ] || ! [[ $dr =~ $re ]]; then
    while true; do
        read -p "How often? (q to quit) : " dr
        case $dr in
        [q]* ) exit;;
        esac

        if ! [[ $dr =~ $re ]] ; then
            echo "$dr is not a positive, whole integer."
        else
            break
        fi
    done
fi

execute "$dr" "$method" "$printer"
