/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/CornerFixedGame.hpp"
#include "../include/Random.hpp"
#include <algorithm>

CornerFixedGame::CornerFixedGame(Random& r,
                                 std::size_t w, std::size_t h) noexcept
                : Game(r, w, h),
                  last(Directions::UP)
{
    Game::add(*this);
}

Game::State CornerFixedGame::myUpdate(Board&) noexcept
{
    auto dirs = Game::possibleMoves();
    if(Game::isGameOver())
        return State::LOST;

    auto itUp    = std::find(dirs.begin(), dirs.end(), Directions::UP);
    auto itRight = std::find(dirs.begin(), dirs.end(), Directions::RIGHT);
    auto itDown  = std::find(dirs.begin(), dirs.end(), Directions::DOWN);
    auto itLeft  = std::find(dirs.begin(), dirs.end(), Directions::LEFT);

    Directions dir;
    if(last == Directions::UP || last == Directions::LEFT)
    {
        if(itRight != dirs.end())
            dir = Directions::RIGHT;
        else if(itUp != dirs.end())
            dir = Directions::UP;
        else if(itDown != dirs.end())
            dir = Directions::DOWN;
        else
            dir = Directions::LEFT;
    }
    else
    {
        if(itUp != dirs.end())
            dir = Directions::UP;
        else if(itDown != dirs.end())
            dir = Directions::DOWN;
        else if(itRight != dirs.end())
            dir = Directions::RIGHT;
        else
            dir = Directions::LEFT;
    }
    last = dir;

    Game::move(dir);

    return State::RUNNING;
}

void CornerFixedGame::notify() noexcept
{

}
