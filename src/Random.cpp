/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Random.hpp"
#include <chrono>

Random::Random() noexcept
       : generator()
{
    std::uint_fast32_t seed = 0U;
    try
    {
        std::random_device rd;
        seed = rd(); //might throw
    }
    catch(std::exception& e)
    {
        seed = std::chrono::system_clock::now().time_since_epoch().count();
    }

    generator.seed(seed);
}

std::mt19937& Random::gen() noexcept
{
    return generator;
}
