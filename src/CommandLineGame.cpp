/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/CommandLineGame.hpp"
#include "../include/CmdPrinter.hpp"
#include <iostream>
#include <iomanip>

CommandLineGame::CommandLineGame(Random& r,
                                 std::size_t w, std::size_t h) noexcept
                : Game(r, w, h),
                  quit(false)
{
    Game::add(*this);
}

Game::State CommandLineGame::myUpdate(Board&) noexcept
{
    Directions dir = askForDirection();

    if(quit)
        return State::QUIT;
    Game::move(dir);

    if(Game::isGameOver())
        return State::LOST;

    return State::RUNNING;
}

void CommandLineGame::notify() noexcept
{

}

Directions CommandLineGame::askForDirection() noexcept
{
    bool goodAnswer = false;
    Directions dir = Directions::UP;

    while(!goodAnswer)
    {
        std::cout << "(Uu)p, (Ll)eft, (Rr)ight, (Dd)own or (Qq)uit\t";
        std::cout << "Answer: ";
        
        std::string answerS;
        std::getline(std::cin, answerS);

        if(answerS.size() != 1U)
            continue;
        
        const char answer = std::toupper(answerS.front());

        if(answer == 'U')
            dir = Directions::UP;
        else if(answer == 'L')
            dir = Directions::LEFT;
        else if(answer == 'R')
            dir = Directions::RIGHT;
        else if(answer == 'D')
            dir = Directions::DOWN;
        else if(answer == 'Q')
            quit = true;
        else
            continue;

        goodAnswer = true;
    }

    return dir;
}
