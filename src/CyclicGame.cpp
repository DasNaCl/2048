/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/CyclicGame.hpp"
#include <algorithm>

CyclicGame::CyclicGame(Random& r,
                       std::size_t w, std::size_t h) noexcept
           : Game(r, w, h),
             cycle({Directions::UP, Directions::LEFT,
                    Directions::UP, Directions::RIGHT}),
             cur(cycle.begin())
{
    Game::add(*this);
}

Game::State CyclicGame::myUpdate(Board&) noexcept
{
    auto dirs = Game::possibleMoves();

    auto curcpy = cur;
    for(; curcpy != cycle.end(); ++curcpy)
    {
        auto it = std::find(dirs.begin(), dirs.end(), *curcpy);
        
        if(it != dirs.end())
        {
            Game::move(*curcpy);
            ++cur;

            if(cur == cycle.end())
                cur = cycle.begin();
            return State::RUNNING;
        }
    }
    for(curcpy = cycle.begin(); curcpy != cur; ++curcpy)
    {
        auto it = std::find(dirs.begin(), dirs.end(), *curcpy);
        
        if(it != dirs.end())
        {
            Game::move(*curcpy);
            ++cur;

            if(cur == cycle.end())
                cur = cycle.begin();
            return State::RUNNING;
        }
    }

    auto it = std::find(dirs.begin(), dirs.end(), Directions::DOWN);
    if(it != dirs.end())
    {
        Game::move(Directions::DOWN);
        cur = cycle.begin();

        return State::RUNNING;
    }

    return State::LOST;
}

void CyclicGame::notify() noexcept
{

}
