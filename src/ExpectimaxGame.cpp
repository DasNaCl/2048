/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/ExpectimaxGame.hpp"
#include <algorithm>
#include <cassert>
#include <numeric>

ExpectimaxGame::ExpectimaxGame(Random& r,
                               std::size_t w, std::size_t h) noexcept
               : Game(r, w, h),
                 depth(0U), weights(1.f, 1.f),
                 snake{ 10.f, 8.0f, 7.0f, 6.5f,
                        0.5f, 0.7f, 1.0f, 3.0f,
                       -0.5f,-1.5f,-1.8f,-2.0f,
                       -3.8f,-3.7f,-3.5f,-3.0f }
{
    std::transform(snake.begin(), snake.end(), snake.begin(),
                  [](float a){ return std::exp(a); });

    //Works with 16 tiles only (for now)
    const auto dim = Game::board().dim();
    assert(dim.first * dim.second == snake.size());
}

ExpectimaxGame::Node::Node(const std::vector<Directions>& Path,
                           float Probability, float Score,
                           Board& Board) noexcept
                     : path(Path), probability(Probability),
                       score(Score), board(Board), children()
{

}

void ExpectimaxGame::notify() noexcept
{

}

Game::State ExpectimaxGame::myUpdate(Board& b) noexcept
{
    const std::size_t freeFieldCount = b.freeFieldCount();
    depth = (freeFieldCount > 7U ? 1U : (freeFieldCount > 4U ? 2U : 3U));

    Node root({}, 1.f, 0.f, b);
    expandMoves(root);

    if(root.children.empty())
        return State::LOST;

    std::vector<float> results;
    results.resize(root.children.size());
    std::transform(root.children.begin(), root.children.end(),
                   results.begin(),
            [this](Node& n){ return expectimax(n); });

    const auto maxIterator = std::max_element(results.begin(), results.end());
    const std::size_t maxIndex = std::distance(results.begin(), maxIterator);

    Game::move(root.children[maxIndex].path.front());

    return State::RUNNING;
}

float ExpectimaxGame::expectimax(Node& n) const noexcept
{
    if(n.children.empty())
        return n.score;
    
    std::vector<float> results;
    results.resize(n.children.size());
    std::transform(n.children.begin(), n.children.end(), results.begin(),
                   [this](Node& n){ return expectimax(n); });

    if(!(n.probability >= 0.92f))
    {
        auto res = std::max_element(results.begin(), results.end());

        return *res;
    }

    float avg = 0.f;
    for(std::size_t i = 0U; i < results.size(); ++i)
    {
        const float prob = n.children[i].probability;
        const float valu = results[i];

        avg += prob * valu;
    }

    return (avg / (results.size() / 2.f));
}

float ExpectimaxGame::expandMoves(Node& n) const noexcept
{
    bool isLeaf = true;
    float x = 0.f;

    if(n.path.size() < depth)
    {
        constexpr const auto dirsC = {Directions::UP, Directions::RIGHT,
                                      Directions::DOWN, Directions::LEFT};
        std::vector<Directions> dirs;
        std::copy_if(std::begin(dirsC), std::end(dirsC),
                     std::back_inserter(dirs),
                     [&n](Directions d)
                     { return Game::isMovePossible(d, n.board); });
        
        isLeaf = dirs.empty();

        std::for_each(dirs.begin(), dirs.end(),
        [this,&n,&x](Directions dir)
        {
            auto cpy = n.board;

            switch(dir)
            {
            case Directions::UP:    cpy.justMove(VerticalDirections::UP);      break;
            case Directions::RIGHT: cpy.justMove(HorizontalDirections::RIGHT); break;
            case Directions::DOWN:  cpy.justMove(VerticalDirections::DOWN);    break;
            case Directions::LEFT:  cpy.justMove(HorizontalDirections::LEFT);  break;
            }

            n.children.emplace_back(n.path, 1.f, 0.f, cpy);
            n.children.back().path.push_back(dir);

            x += expandRandom(n.children.back());
        });
    }
    
    if(isLeaf)
        n.score = calcScore(n.board);
    return (isLeaf ? 1.f : x);
}

float ExpectimaxGame::expandRandom(Node& n) const noexcept
{
    const auto dim = n.board.dim();

    float c = 0.f;
    for(std::size_t y = 0U; y < dim.second; ++y)
    {
        for(std::size_t x = 0U; x < dim.first; ++x)
        {
            auto& t = n.board.getTile(x, y);
            
            if(!t.isSet())
            {
                auto cpy = n.board;
                auto& nt = cpy.getTile(x, y);

                nt.set(1U);

                n.children.emplace_back(n.path, 0.9f, 0.f, cpy);
                c += expandMoves(n.children.back());

                nt.unset().set(2U);

                n.children.emplace_back(n.path, 0.1f, 0.f, cpy);
                c += expandMoves(n.children.back());
            }
        }
    }
    
    return c;
}

float ExpectimaxGame::calcScore(Board& board) const noexcept
{
    const std::size_t freeFieldCount = board.freeFieldCount();
    const std::size_t freeFieldCountSquared = freeFieldCount * freeFieldCount;
    const auto dim = board.dim();
    
    float c = 0.f;
    for(std::size_t y = 0U; y < dim.second; ++y)
    {
        for(std::size_t x = 0U; x < dim.first; ++x)
        {
            const auto& t = board.getTile(x, y);
            if(t.isSet())
            {
                const float a = static_cast<float>(t.val());
                c += a * snake[x + y * dim.first];
            }
        }
    }

    return c * weights.first + freeFieldCountSquared * weights.second;
}
