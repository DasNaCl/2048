/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/MaxScoreGame.hpp"

MaxScoreGame::MaxScoreGame(Random& r,
                           std::size_t w, std::size_t h) noexcept
             : Game(r, w, h)
{
    Game::add(*this);
}

Game::State MaxScoreGame::myUpdate(Board&) noexcept
{
    auto dirs = Game::possibleMoves();
    if(dirs.empty())
        return State::LOST;

    Directions chosen = dirs.front();
    
    const std::size_t currentPoints = Game::board().totalPoints();
    std::size_t maxPoints = 0U;
    for(const auto& d : dirs)
    {
        auto cpy = Game::board();

        if(d == Directions::UP)
            cpy.move(VerticalDirections::UP);
        else if(d == Directions::RIGHT)
            cpy.move(HorizontalDirections::RIGHT);
        else if(d == Directions::DOWN)
            cpy.move(VerticalDirections::DOWN);
        else if(d == Directions::LEFT)
            cpy.move(HorizontalDirections::LEFT);

        std::size_t pnts = cpy.totalPoints() - currentPoints;
        
        if(pnts > maxPoints)
        {
            maxPoints = pnts;
            chosen = d;
        }
    }

    Game::move(chosen);
    
    return State::RUNNING;
}

void MaxScoreGame::notify() noexcept
{

}
