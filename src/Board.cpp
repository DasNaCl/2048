/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Board.hpp"
#include "../include/Random.hpp"
#include "../include/Exceptions.hpp"
#include <algorithm>
#include <cassert>
#include <random>

Board::Board(Random& random,
             std::size_t w, std::size_t h) noexcept
      : vec(),
        dimension(w, h),
        rnd(random),
        points(0U),
        pointsLastRound(0U),
        collisionLastRound(false)
{
    vec.resize(dimension.first * dimension.second);

    setInitialTiles();
}

Board::Board(Random& random,
             Vector2s bounds) noexcept
      : vec(),
        dimension(bounds),
        rnd(random),
        points(0U),
        pointsLastRound(0U),
        collisionLastRound(false)
{
    vec.resize(dimension.first * dimension.second);

    setInitialTiles();
}

void Board::reset() noexcept
{
    points = 0U;
    pointsLastRound = 0U;
    collisionLastRound = false;

    for(auto& t : vec)
        t.unset();

    setInitialTiles();
}

Tile& Board::getTile(Board::Vector2s pos) noexcept
{
    return getTile(pos.first, pos.second);
}

Tile& Board::getTile(std::size_t x, std::size_t y) noexcept
{
    return vec[x + y * dimension.first];
}

const Tile& Board::getTile(Board::Vector2s pos) const noexcept
{
    return getTile(pos.first, pos.second);
}

const Tile& Board::getTile(std::size_t x, std::size_t y) const noexcept
{
    return vec[x + y * dimension.first];
}

Board::Vector2s Board::dim() const noexcept
{
    return dimension;
}

void Board::justMove(VerticalDirections dir) noexcept
{
    prepareToMove();

    if(dir == VerticalDirections::UP)
    {
        for(std::size_t y = 0U; y < dimension.second; ++y)
            for(std::size_t x = 0U; x < dimension.first; ++x)
                verticalMove(x, y, dir);
    }
    else
    {
        constexpr const auto max = static_cast<std::size_t>(-1);
        for(std::size_t y = dimension.second-1U; y < max; --y)
            for(std::size_t x = 0U; x < dimension.first; ++x)
                verticalMove(x, y, dir);
    }
}

void Board::move(VerticalDirections dir) noexcept
{
    auto cpy = *this;

    justMove(dir);

    if(isDifferent(cpy))
        addTile();

    notify();
}

void Board::justMove(HorizontalDirections dir) noexcept
{
    prepareToMove();
    
    if(dir == HorizontalDirections::LEFT)
    {
        for(std::size_t y = 0U; y < dimension.second; ++y)
            for(std::size_t x = 0U; x < dimension.first; ++x)
                horizontalMove(x, y, dir);
    }
    else
    {
        constexpr const auto max = static_cast<std::size_t>(-1);
        for(std::size_t y = 0U; y < dimension.second; ++y)
            for(std::size_t x = dimension.first-1U; x < max; --x)
                horizontalMove(x, y, dir);
    }
}

void Board::move(HorizontalDirections dir) noexcept
{
    auto cpy = *this;

    justMove(dir);

    if(isDifferent(cpy))
        addTile();

    notify();
}

std::size_t Board::pointsScoredLastRound() const noexcept
{
    return pointsLastRound;
}

std::size_t Board::totalPoints() const noexcept
{
    return points;
}

bool Board::didTileCollideLastRound() const noexcept
{
    return collisionLastRound;
}

std::size_t Board::freeFieldCount() const noexcept
{
    return std::accumulate(vec.begin(), vec.end(), 0U,
            [](std::size_t cur, const Tile& t)
            {
                return cur + ((!t.isSet()) ? 1U : 0U);
            });
}

std::size_t Board::boxSize() const
{
    const std::size_t val = (2U << maxExp()-1U);
    const auto vals = std::to_string(val);
    
    return vals.size();
}

std::size_t Board::maxExp() const noexcept
{
    std::size_t max = std::find_if(vec.begin(), vec.end(),
                                   [](const Tile& t)
                                   {
                                       return t.isSet();
                                   })->exp();
    for(const auto& t : vec)
        if(t.isSet() && t.exp() > max)
            max = t.exp();
    return max;
}

bool Board::isFull() const noexcept
{
    for(const auto& t : vec)
        if(!t.isSet())
            return false;
    return true;   
}

bool Board::anyMovePossible() const noexcept
{
    if(isFull())
    {
        bool canMerge = false;
        for(std::size_t y = 0U; y < dimension.second; ++y)
        {
            for(std::size_t x = 0U; x < dimension.first; ++x)
            {
                const std::size_t center = x + y * dimension.first;
                
                if(!vec[center].isSet())
                    continue;
                
                const std::size_t up    = x + (y - 1U) * dimension.first;
                const std::size_t left  = (x - 1U) + y * dimension.first;
                const std::size_t down  = x + (y + 1U) * dimension.first;
                const std::size_t right = (x + 1U) + y * dimension.first;

                if(up < vec.size() && vec[up].isSet()
                && vec[center].exp() == vec[up].exp())
                    return true;

                if(left < vec.size() && vec[left].isSet()
                && vec[center].exp() == vec[left].exp())
                    return true;

                if(down < vec.size() && vec[down].isSet()
                && vec[center].exp() == vec[down].exp())
                    return true;

                if(right < vec.size() && vec[right].isSet()
                && vec[center].exp() == vec[right].exp())
                    return true;
            }
        }

        return false;
    }

    return true;
}

bool Board::isMovePossible(VerticalDirections dir) noexcept
{
    auto cpy = *this;
    cpy.move(dir);

    return isDifferent(cpy);
}

bool Board::isMovePossible(HorizontalDirections dir) noexcept
{
    auto cpy = *this;
    cpy.move(dir);

    return isDifferent(cpy);
}

void Board::prepareToMove() noexcept
{
    collisionLastRound = false;
    pointsLastRound = 0U;

    for(auto& t : vec)
        t.didntUpgrade();
}

void Board::addTile()
{
    // 4 has a 10% chance to appear
    std::bernoulli_distribution expd(0.1);

    auto freePos = freePosition();
    auto& t = vec[freePos.first + freePos.second * dimension.first];

    t.set(expd(rnd.gen()) ? 2 : 1);
}

Board::Vector2s Board::freePosition() const
{
    if(!anyMovePossible())
        throw NoMovePossibleException();

    std::vector<Vector2s> free;
    for(std::size_t y = 0U; y < dimension.second; ++y)
        for(std::size_t x = 0U; x < dimension.first; ++x)
            if(!vec[x + y * dimension.first].isSet())
                free.push_back({x, y});
    assert(free.size() > 0);

    std::uniform_int_distribution<std::size_t> dist(0U, free.size()-1U);
    
    return free[dist(rnd.gen())];
}

bool Board::isDifferent(Board& board) const noexcept
{
    if(dimension != board.dimension)
        return false;
    
    const std::size_t lim = dimension.first * dimension.second;
    for(std::size_t i = 0U; i < lim; ++i)
    {
        bool tileAtSamePos = !(vec[i].isSet() ^ board.vec[i].isSet());
        
        if(!tileAtSamePos)
            return true;

        bool sameValues = vec[i].exp() == board.vec[i].exp();

        if(!sameValues)
            return true;
    }

    return false;
}

void Board::horizontalMove(std::size_t x, std::size_t y,
                           HorizontalDirections dir) noexcept
{
    const int d = (dir == HorizontalDirections::RIGHT ? 1 : -1);
    const auto oldPos = x + y * dimension.first;
    if(vec[oldPos].isSet())
    {
        int tx = x + d;

        while(0 < tx && tx < dimension.first
           && !vec[tx + y * dimension.first].isSet())
        {
            tx = tx + d;
        }
        
        const std::size_t newPos = tx + y * dimension.first;
        if(0 <= tx && tx < dimension.first)
        {
            if(vec[newPos].isSet())
            {
                if(vec[newPos].exp() == vec[oldPos].exp()
                && !vec[newPos].didItUpgrade())
                {
                    vec[oldPos].unset();
                    vec[newPos].increaseExp();
                    vec[newPos].didUpgrade();

                    points += vec[newPos].val();
                    pointsLastRound += vec[newPos].val();
                }
                else
                {
                    std::swap(vec[oldPos], vec[newPos - d]);
                }
            }
            else
            {
                std::swap(vec[oldPos], vec[newPos]);
            }
        }
        else
        {
            std::swap(vec[oldPos], vec[newPos - d]);
        }
    }
}

void Board::horizontalMove(Vector2s pos,
                            HorizontalDirections dir) noexcept
{
    horizontalMove(pos.first, pos.second, dir);
}

void Board::verticalMove(std::size_t x, std::size_t y,
                         VerticalDirections dir) noexcept
{
    const int d = (dir == VerticalDirections::DOWN ? 1 : -1);
    const auto oldPos = x + y * dimension.first;
    if(vec[oldPos].isSet())
    {
        int ty = y + d;

        while(0 < ty && ty < dimension.second
           && !vec[x + ty * dimension.first].isSet())
        {
            ty = ty + d;
        }
        
        const std::size_t newPos = x + ty * dimension.first;
        if(0 <= ty && ty < dimension.second)
        {
            if(vec[newPos].isSet())
            {
                if(vec[newPos].exp() == vec[oldPos].exp()
                && !vec[newPos].didItUpgrade())
                {
                    vec[oldPos].unset();
                    vec[newPos].increaseExp();
                    vec[newPos].didUpgrade();

                    points += vec[newPos].val();
                    pointsLastRound += vec[newPos].val();
                }
                else
                {
                    std::swap(vec[oldPos], vec[newPos - d*dimension.first]);
                }
            }
            else
            {
                std::swap(vec[oldPos], vec[newPos]);
            }
        }
        else
        {
            std::swap(vec[oldPos], vec[newPos - d*dimension.first]);
        }
    }
}

void Board::verticalMove(Vector2s pos,
                         VerticalDirections dir) noexcept
{
    verticalMove(pos.first, pos.second, dir);
}

void Board::setInitialTiles()
{
    addTile();
    addTile();
}
