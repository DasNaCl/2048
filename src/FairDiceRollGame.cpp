/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/FairDiceRollGame.hpp"
#include "../include/Random.hpp"
#include <cassert>

FairDiceRollGame::FairDiceRollGame(Random& r,
                                   std::size_t w, std::size_t h) noexcept
                 : Game(r, w, h),
                   rnd(r)
{
    Game::add(*this);
}

Game::State FairDiceRollGame::myUpdate(Board& b) noexcept
{
    auto dirs = possibleMoves();

    if(dirs.empty())
        return Game::State::LOST;
    
    std::uniform_int_distribution<std::size_t> dist(0U, dirs.size());
    std::size_t result = dist(rnd.gen());
    
    Game::move(dirs[result]);  
    
    return State::RUNNING;
}

void FairDiceRollGame::notify() noexcept
{
    
}
