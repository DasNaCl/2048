/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/ListenerManager.hpp"
#include "../include/Listener.hpp"

ListenerManager::ListenerManager() noexcept
                : listeners()
{

}

ListenerManager::~ListenerManager() noexcept
{

}

void ListenerManager::notify() noexcept
{
    for(auto& l : listeners)
        l.get().notify();
}

void ListenerManager::add(Listener& lst) noexcept
{
    listeners.push_back(std::ref(lst));
}
