/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Application.hpp"
#include "../include/Exceptions.hpp"

#include "../include/FairDiceRollGame.hpp"
#include "../include/CommandLineGame.hpp"
#include "../include/CornerFixedGame.hpp"
#include "../include/ExpectimaxGame.hpp"
#include "../include/MinScoreGame.hpp"
#include "../include/MaxScoreGame.hpp"
#include "../include/CyclicGame.hpp"

#include "../include/BiggestTilePrinter.hpp"
#include "../include/NothingPrinter.hpp"
#include "../include/ScoresPrinter.hpp"
#include "../include/LaTeXPrinter.hpp"
#include "../include/CountPrinter.hpp"
#include "../include/CmdPrinter.hpp"

#include <iostream>
#include <fstream>
#include <sstream>
#include <iomanip>
#include <limits>
#include <array>
#include <queue>

Application::Application() noexcept
            : gam(), printer(), r(),
              gameFactory([this]()
              { return std::make_unique<CommandLineGame>(r, 4, 4); }),
              printerFactory([this]()
              { return std::make_unique<NothingPrinter>(*gam); }),
              printWin(false),
              dontPrintWin(false)
{

}

int Application::run(const std::vector<std::string>& arguments) noexcept
{
    UserOpinion res = UserOpinion::OK;

    try
    {
        res = readArguments(arguments);
    }
    catch(std::invalid_argument& e)
    {
        std::cerr << e.what() << std::endl;

        return 0;
    }
    catch(std::exception& e)
    {
        std::cerr << e.what() << std::endl;

        return -1;
    }

    if(res == UserOpinion::HELP)
        return 0;

    gam = gameFactory();
    printer = printerFactory();

    gam->notify();

    Game::State st = Game::State::RUNNING;
    do
    {
        st = gam->update();
    } while(st == Game::State::RUNNING);

    if(!printer->didPrint())
        gam->notify();

    if(!dontPrintWin)
    {
        if(!printWin)
            std::cout << (gam->won() ? "Tough luck, you've lost!"
                                     : "Good bye!") << std::endl;
        else
            std::cout << (gam->won() ? "1" : "0") << std::endl;
    }

    return 0;
}

auto Application::readArguments(const std::vector<std::string>& args)
    -> Application::UserOpinion
{
    bool gameSet = false;
    bool printerSet = false;
    for(auto it = args.cbegin(); it != args.cend(); ++it)
    {
        const auto arg = *it;
        if(arg == "-h" || arg == "--help")
        {
            printHelp();

            return UserOpinion::HELP;
        }
        else if(arg == "-wx" || arg == "--winning-exp")
        {
            auto next = ++it;
            
            if(next == args.cend())
                throw std::invalid_argument("\""+arg+"\" expected an input.");

            std::string newarg = *next;
            std::size_t val = 0U;
            std::stringstream ss(newarg);
            if(!(ss >> val) || newarg[0] == '-')
                throw std::invalid_argument("\""+newarg+"\" can't be converted to"
                                            + " a positive, whole integer.");

            if(val < 1)
                throw std::invalid_argument("\""+newarg+"\" is not in the range 0 < X.");

            Game::winningExp = val;
        }
        else if(arg == "-cls" || arg == "--command-line-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<CommandLineGame>(r, 4U, 4U); };

                gameSet = true;
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-cfs" || arg == "--corner-fixed-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<CornerFixedGame>(r, 4U, 4U); };

                gameSet = true;
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-ccs" || arg == "--cyclic-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<CyclicGame>(r, 4U, 4U); };

                gameSet = true;
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-uds" || arg == "--uniform-dist-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<FairDiceRollGame>(r, 4U, 4U); };

                gameSet = true;
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-miss" || arg == "--min-score-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<MinScoreGame>(r, 4U, 4U); };
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-mass" || arg == "--max-score-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<MaxScoreGame>(r, 4U, 4U); };
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-exp" || arg == "--expectimax-strategy")
        {
            if(!gameSet)
            {
                gameFactory = [this]()
                { return std::make_unique<ExpectimaxGame>(r, 4U, 4U); };
            }
            else
                throw GameAlreadySet();
        }
        else if(arg == "-lat" || arg == "--as-latex")
        {
            if(!printerSet)
            {
                printerFactory = [this]()
                { return std::make_unique<LaTeXPrinter>(*gam); };

                printerSet = true;
            }
            else
                throw PrinterAlreadySet();
        }
        else if(arg == "-asc" || arg == "--as-ascii")
        {
            if(!printerSet)
            {
                printerFactory = [this]()
                { return std::make_unique<CmdPrinter>(*gam); };
            }
            else
                throw PrinterAlreadySet();
        }
        else if(arg == "-amo" || arg == "--as-amount")
        {
            if(!printerSet)
            {
                printerFactory = [this]()
                { return std::make_unique<CountPrinter>(*gam); };
            }
            else
                throw PrinterAlreadySet();
        }
        else if(arg == "-sae" || arg == "--score-at-end")
        {
            if(!printerSet)
            {
                printerFactory = [this]()
                { return std::make_unique<ScoresPrinter>(*gam); };
            }
            else
                throw PrinterAlreadySet();
        }
        else if(arg == "-tae" || arg == "--tile-at-end")
        {
            if(!printerSet)
            {
                printerFactory = [this]()
                { return std::make_unique<BiggestTilePrinter>(*gam); };
            }
            else
                throw PrinterAlreadySet();
        }
        else if(arg == "-pw" || arg == "--print-win")
        {
            printWin = true;
        }
        else if(arg == "-dpw" || arg == "--dont-print-win")
        {
            dontPrintWin = true;
        }
        else
        {
            throw std::invalid_argument("\""+arg+"\" is unknown.");
        }
    }

    return UserOpinion::OK;
}

void Application::printHelp() const noexcept
{
    std::cout << "2048 -[wx|cls|cfs|ccs|uds|miss|mass|exp|lat|asc|amo|sae|tae|pw|dpw]..." << std::endl;

    constexpr const auto printElement =
        [](std::string shortVer, std::string longVer,
                std::string briefDesc)
        {
            std::cout << std::right << std::setw(5U);
            std::cout << shortVer;
            std::cout << std::left << std::setw(27U);
            std::cout << longVer;
            std::cout << std::setw(3U) << " | " << std::left;
            std::cout << briefDesc << std::endl;
        };
    
    printElement("-h", " --help",
                "Print this help.");

    printElement("-wx", " --winning-exp",
                "Change the winning exponent. (default is 11)");

    printElement("-cls", " --command-line-strategy",
                "Play the game by yourself. (default)");

    printElement("-cfs", " --corner-fixed-strategy",
                "Let AI try to move everything into a corner.");

    printElement("-ccs", " --cyclic-strategy",
                "Let AI move in a specific pattern. (URUL, if necessary D)");

    printElement("-uds", " --uniform-dist-strategy",
                "Let AI choose it's turn by fair dice roll.");

    printElement("-miss", " --min-score-strategy",
                "Let AI choose it's turn by minimizing the next score.");

    printElement("-mass", " --max-score-strategy",
                "Let AI choose it's turn by maximizing the next score.");

    printElement("-exp", " --expectimax-strategy",
                "Let AI choose it's turn by evaluating an expectimax tree.");

    printElement("-lat", " --as-latex",
                "Print as LaTeX to STDOUT.");

    printElement("-asc", " --as-ascii",
                "Print as ASCII-Art to STDOUT.");

    printElement("-amo", " --as-amount",
                "Just print the fields line by line.");

    printElement("-sae", " --score-at-end",
                "Just print the score at the end of the game.");

    printElement("-tae", " --tile-at-end",
                "Just print the biggest tile score at the end of the game.");

    printElement("-pw", " --print-win",
                "Print whether it won or not as 1/0 instead of text.");

    printElement("-dpw", " --dont-print-win",
                "Do not print any win or defeat.");
}
