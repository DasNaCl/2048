/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Tile.hpp"
#include <cassert>

unsigned char Tile::noexp = static_cast<unsigned char>(-1);

Tile::Tile() noexcept
     : upgrade(false),
       expo(noexp)
{

}

Tile& Tile::set(unsigned char exponent) noexcept
{
    expo = exponent;

    assert(expo != noexp && expo > 0);

    return *this;
}

Tile& Tile::unset() noexcept
{
    expo = noexp;

    return *this;
}

bool Tile::isSet() const noexcept
{
    return expo != noexp;
}

std::size_t Tile::exp() const noexcept
{
    if(expo == noexp)
        return 0;
    return expo;
}

std::size_t Tile::val() const noexcept
{
    const std::size_t exp = static_cast<std::size_t>(expo);

    return (2U << (exp-1U));
}

void Tile::increaseExp() noexcept
{
    ++expo;

    assert(expo != noexp);
}

void Tile::didUpgrade() noexcept
{
    upgrade = true;
}

void Tile::didntUpgrade() noexcept
{
    upgrade = false;
}

bool Tile::didItUpgrade() const noexcept
{
    return upgrade;
}
