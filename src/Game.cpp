/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/Game.hpp"
#include <iomanip>
#include <string>

std::size_t Game::winningExp = 11U; // 2^{11} = 2048

Game::Game(Random& rnd,
           std::size_t width, std::size_t height) noexcept
     : b(rnd, width, height),
       gameOver(false)
{
    restart();
}

Game::~Game()
{

}

Game::State Game::update() noexcept
{
    if(gameOver)
        return State::LOST;

    Game::State result = myUpdate(b);

    gameOver = (result != State::RUNNING);

    notify();

    return result;
}

void Game::restart() noexcept
{
    if(gameOver)
    {
        b.reset();
        gameOver = false;
    }

    notify();
}

void Game::move(Directions dir) noexcept
{
    switch(dir)
    {
    case Directions::UP:    b.move(VerticalDirections::UP);      break;
    case Directions::RIGHT: b.move(HorizontalDirections::RIGHT); break;
    case Directions::DOWN:  b.move(VerticalDirections::DOWN);    break;
    case Directions::LEFT:  b.move(HorizontalDirections::LEFT);  break;
    }

    if(!gameOver && !b.anyMovePossible())
        gameOver = true;
}

void Game::justMove(Directions dir) noexcept
{
    switch(dir)
    {
    case Directions::UP:    b.justMove(VerticalDirections::UP);      break;
    case Directions::RIGHT: b.justMove(HorizontalDirections::RIGHT); break;
    case Directions::DOWN:  b.justMove(VerticalDirections::DOWN);    break;
    case Directions::LEFT:  b.justMove(HorizontalDirections::LEFT);  break;
    }

    if(!gameOver && !b.anyMovePossible())
        gameOver = true;
}

void Game::move(VerticalDirections dir) noexcept
{
    if(dir == VerticalDirections::UP)
        move(Directions::UP);
    else
        move(Directions::DOWN);

    if(!gameOver && !b.anyMovePossible())
        gameOver = true;
}

void Game::move(HorizontalDirections dir) noexcept
{
    if(dir == HorizontalDirections::RIGHT)
        move(Directions::RIGHT);
    else
        move(Directions::LEFT);

    if(!gameOver && !b.anyMovePossible())
        gameOver = true;
}

bool Game::isMovePossible(Directions dir) noexcept
{
    switch(dir)
    {
    case Directions::UP:    return b.isMovePossible(VerticalDirections::UP);
    case Directions::RIGHT: return b.isMovePossible(HorizontalDirections::RIGHT);
    case Directions::DOWN:  return b.isMovePossible(VerticalDirections::DOWN);
    case Directions::LEFT:  return b.isMovePossible(HorizontalDirections::LEFT);
    }
}

bool Game::isMovePossible(Directions dir, Board& board) noexcept
{
    switch(dir)
    {
    case Directions::UP:    return board.isMovePossible(VerticalDirections::UP);
    case Directions::RIGHT: return board.isMovePossible(HorizontalDirections::RIGHT);
    case Directions::DOWN:  return board.isMovePossible(VerticalDirections::DOWN);
    case Directions::LEFT:  return board.isMovePossible(HorizontalDirections::LEFT);
    }
}

bool Game::isGameOver() const noexcept
{
    return gameOver;
}

bool Game::won() const noexcept
{
    return b.maxExp() >= winningExp;
}

std::size_t Game::score() const noexcept
{
    return b.totalPoints();
}

Board& Game::board() noexcept
{
    return b;
}

const Board& Game::board() const noexcept
{
    return b;
}

std::vector<Directions> Game::possibleMoves() noexcept
{
    std::vector<Directions> dirs { Directions::UP,
                                   Directions::RIGHT,
                                   Directions::DOWN,
                                   Directions::LEFT };
    for(auto it = dirs.begin(); it != dirs.end(); )
    {
        auto v = *it;

        bool impossible = (v == Directions::UP
                  && !Game::board().isMovePossible(VerticalDirections::UP));
        impossible = impossible || (v == Directions::RIGHT
                  && !Game::board().isMovePossible(HorizontalDirections::RIGHT));
        impossible = impossible || (v == Directions::DOWN
                  && !Game::board().isMovePossible(VerticalDirections::DOWN));
        impossible = impossible || (v == Directions::LEFT
                  && !Game::board().isMovePossible(HorizontalDirections::LEFT));

        if(impossible)
            it = dirs.erase(it);
        else
            ++it;
    }
    if(dirs.empty())
        gameOver = true;

    return dirs;
}
