/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/CmdPrinter.hpp"
#include "../include/Game.hpp"
#include <iostream>
#include <iomanip>

CmdPrinter::CmdPrinter(Game& ref) noexcept
           : Printer(ref)
{

}

bool CmdPrinter::onPrint() const noexcept
{
    std::cout << *this << std::endl;

    return true;
}

std::ostream& operator<<(std::ostream& os, const CmdPrinter& p)
{
    const auto dimension = p.ref.board().dim();
    const std::size_t boxSize = p.ref.board().boxSize();
    
    std::cout << std::setw(boxSize * (dimension.first / 2U));
    std::cout << " " << std::left << "Score: ";
    std::cout << p.ref.score();
    
    if(p.ref.won())
        std::cout << "*";
    std::cout << std::endl << std::endl;

    for(std::size_t y = 0U; y < dimension.second; ++y)
    {
        for(std::size_t x = 0U; x < dimension.first; ++x)
        {
            auto& t = p.ref.board().getTile(x, y);
            
            os << "|" << std::right << std::setw(boxSize);
            if(t.isSet())
                os << t.val();
            else
                os << " ";
        }
        os << "|";
        
        if(y+1U < dimension.second)
            os << "\n";
    }

    return os;
}
