/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/LaTeXPrinter.hpp"
#include "../include/Game.hpp"
#include <iostream>

LaTeXPrinter::LaTeXPrinter(Game& g) noexcept
             : Printer(g)
{

}

bool LaTeXPrinter::onPrint() const noexcept
{
    std::cout << *this << std::endl;

    return true;
}

std::ostream& operator<<(std::ostream& os, const LaTeXPrinter& p)
{
    const auto dimension = p.ref.board().dim();
    const std::size_t boxSize = p.ref.board().boxSize();
    const auto& b = p.ref.board();

    std::cout << "\\begin{tikzpicture}\n";
    std::cout << "\t\\def\\pixels{\n";

    for(std::size_t y = 0U; y < dimension.second; ++y)
    {
        std::cout << "\t{";
        for(std::size_t x = 0U; x < dimension.first; ++x)
        {
            auto& t = b.getTile(x, y);

            if(t.isSet())
                std::cout << b.getTile(x, y).val();
            else
                std::cout << "0";

            if(x + 1U < dimension.first)
                std::cout << ",";
        }
        
        if(y + 1U < dimension.second)
            std::cout << "},\n";
        else
            std::cout << "},\n}\n";
    }

    std::cout << "\\foreach \\line [count=\\y] in \\pixels {\n";
    std::cout << "\t\\foreach \\pix [count=\\x] in \\line {\n";
    std::cout << "\t\t\\path (\\x,-\\y) node[name=c2048-\\x-\\y,";
    std::cout << "case 2048 \\pix];\n";
    std::cout << "\t}\n}\n";
    std::cout << "\\begin{scope}[on background layer]\n";
    std::cout << "\t\\node[fill=grid color,fit=(c2048-1-1)(c2048-4-4),\n";
    std::cout << "\t\\tinner sep=1mm,rounded corners=.3mm]{};\n";
    std::cout << "\\end{scope}\n\\end{tikzpicture}";
}
