/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "../include/CountPrinter.hpp"
#include "../include/Game.hpp"
#include <iostream>

CountPrinter::CountPrinter(Game& g) noexcept
             : Printer(g)
{

}

bool CountPrinter::onPrint() const noexcept
{
    std::cout << *this << std::endl;

    return ref.isGameOver();
}

std::ostream& operator<<(std::ostream& os, const CountPrinter& p)
{
    if(p.ref.isGameOver())
        return os;

    const auto dimension = p.ref.board().dim();
    const auto& b = p.ref.board();

    for(std::size_t y = 0U; y < dimension.second; ++y)
    {
        for(std::size_t x = 0U; x < dimension.first; ++x)
        {
            auto& t = b.getTile(x, y);

            if(t.isSet())
            {
                os << t.val();
            
                if(x + y * dimension.second+1 < dimension.first*dimension.second)
                    os << std::endl;
            }
        }
    }
    
    return os;
}
