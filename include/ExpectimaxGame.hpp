/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Listener.hpp"
#include "Game.hpp"

class ExpectimaxGame : public Game, public Listener
{
public:
    struct Node
    {
        explicit Node(const std::vector<Directions>& Path,
                      float Probability, float Score,
                      Board& Board) noexcept;
        std::vector<Directions> path;
        float probability;
        float score;
        Board board;
        std::vector<Node> children;
    };
public:
    explicit ExpectimaxGame(Random& r,
                            std::size_t w, std::size_t h) noexcept;
    
    void notify() noexcept override;
private:
    Game::State myUpdate(Board& b) noexcept override;
    float expectimax(Node& n) const noexcept;
    float expandMoves(Node& n) const noexcept;
    float expandRandom(Node& n) const noexcept;
    float calcScore(Board& board) const noexcept;
private:
    std::size_t depth;
    std::pair<float, float> weights;
    std::vector<float> snake;
};
