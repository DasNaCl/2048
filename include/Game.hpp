/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "ListenerManager.hpp"
#include "Board.hpp"
#include <ostream>

enum class Directions
{
    UP,
    RIGHT,
    DOWN,
    LEFT,
};

class CmdPrinter;
class CountPrinter;
class LaTeXPrinter;
class ScoresPrinter;
class BiggestTilePrinter;

class Application;
class Game : public ListenerManager
{
public:
    enum class State
    {
        RUNNING,
        QUIT,
        LOST,
    };
public:
    static std::size_t winningExp;

    friend class Application;
    friend std::ostream& operator<<(std::ostream& os, const CmdPrinter& cmd);
    friend std::ostream& operator<<(std::ostream& os, const LaTeXPrinter& lat);
    friend std::ostream& operator<<(std::ostream& os, const CountPrinter& cnt);
    friend std::ostream& operator<<(std::ostream& os, const ScoresPrinter& sco);
    friend std::ostream& operator<<(std::ostream& os, const BiggestTilePrinter& big);
public:
	explicit Game(Random& rnd,
                  std::size_t width, std::size_t height) noexcept;
    virtual ~Game() noexcept;

    State update() noexcept;

    void restart() noexcept;
    void move(Directions dir) noexcept;
    void justMove(Directions dir) noexcept;
    void move(VerticalDirections dir) noexcept;
    void move(HorizontalDirections dir) noexcept;

    bool isMovePossible(Directions dir) noexcept;
    static bool isMovePossible(Directions dir, Board& board) noexcept;
    
    bool isGameOver() const noexcept;
    bool won() const noexcept;

    std::size_t score() const noexcept;
protected:
    virtual State myUpdate(Board& b) noexcept = 0;

    Board& board() noexcept;
    const Board& board() const noexcept;

    std::vector<Directions> possibleMoves() noexcept;
private:
    Board b;
    bool gameOver;
};
