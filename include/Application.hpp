/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "Game.hpp"
#include "Printer.hpp"
#include "Random.hpp"
#include <functional>
#include <memory>
#include <string>
#include <vector>

class Random;

class Application
{
public:
    enum class UserOpinion
    {
        HELP,
        OK,
    };
public:
    explicit Application() noexcept;

    int run(const std::vector<std::string>& arguments) noexcept;	
private:
    UserOpinion readArguments(const std::vector<std::string>& args);
    void printHelp() const noexcept;
private:
    std::unique_ptr<Game> gam;
    std::unique_ptr<Printer> printer;
    Random r;

    std::function<std::unique_ptr<Game>(void)> gameFactory;
    std::function<std::unique_ptr<Printer>(void)> printerFactory;

    bool printWin;
    bool dontPrintWin;
};
