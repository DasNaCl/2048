/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include <stdexcept>
#include <string>

class NoMovePossibleException : public std::exception
{
    const char* what() const noexcept override
    {
        return "No more move is possible.";
    }
};

class GameAlreadySet : public std::exception
{
    const char* what() const noexcept override
    {
        return "The game was already set. You cannot let two games play within one run.";
    }
};

class PrinterAlreadySet : public std::exception
{
    const char* what() const noexcept override
    {
        return "Although it's logically possible, you can't use multiple printers currently in this program.";
    }
};
