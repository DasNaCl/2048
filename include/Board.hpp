/*
	Copyright (C) 2015 Matthis Kruse

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#pragma once 

#include "ListenerManager.hpp"
#include "Tile.hpp"
#include <utility>
#include <vector>

enum class VerticalDirections
{
    UP,
    DOWN,
};

enum class HorizontalDirections
{
    LEFT,
    RIGHT,
};

class Random;

class Board : public ListenerManager
{
public:
    using Vector2s = std::pair<std::size_t, std::size_t>;
public:
	explicit Board(Random& random,
                   std::size_t w, std::size_t h) noexcept;
    explicit Board(Random& random,
                   Vector2s bounds) noexcept;

    void reset() noexcept;
    Tile& getTile(Vector2s pos) noexcept;
    Tile& getTile(std::size_t x, std::size_t y) noexcept;
    const Tile& getTile(Vector2s pos) const noexcept;
    const Tile& getTile(std::size_t x, std::size_t y) const noexcept;
    Vector2s dim() const noexcept;
    void move(VerticalDirections dir) noexcept;
    void move(HorizontalDirections dir) noexcept;

    void justMove(VerticalDirections dir) noexcept;
    void justMove(HorizontalDirections dir) noexcept;
    
    std::size_t pointsScoredLastRound() const noexcept;
    std::size_t totalPoints() const noexcept;
    bool didTileCollideLastRound() const noexcept;

    std::size_t freeFieldCount() const noexcept;
    std::size_t boxSize() const;
    std::size_t maxExp() const noexcept;
    bool isFull() const noexcept;
    bool anyMovePossible() const noexcept;
    bool isMovePossible(VerticalDirections dir) noexcept;
    bool isMovePossible(HorizontalDirections dir) noexcept;

    bool isDifferent(Board& board) const noexcept;
private:
    void prepareToMove() noexcept;
    void addTile();
    Vector2s freePosition() const;

    void horizontalMove(std::size_t x, std::size_t y,
                        HorizontalDirections dir) noexcept;
    void horizontalMove(Vector2s pos,
                        HorizontalDirections dir) noexcept;

    void verticalMove(std::size_t x, std::size_t y,
                      VerticalDirections dir) noexcept;
    void verticalMove(Vector2s pos,
                      VerticalDirections dir) noexcept;
    void setInitialTiles();
private:
    std::vector<Tile> vec;
    Vector2s dimension;

    Random& rnd;

    std::size_t points;
    std::size_t pointsLastRound;
    bool collisionLastRound;
};
